#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
set -o noglob

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CI_BUILD_DATE=$(date '+%s')
CI_PROJECT_URL="${CI_PROJECT_URL:-https://gitlab.com/leipert/is-gitlab-pretty-yet}"
CI_JOB_NAME="${CI_JOB_NAME:-generate_stats}"

# Apparently eslint analysis can be very expensive, so we give node more memory
export NODE_OPTIONS="--max_old_space_size=4096"

mkdir -p repositories

function log {
>&2 printf "\\e[33m%s\\e[0m\\n" "$1"
}

function download {
  curl  --fail --location --silent --show-error "$@"
}

function indent { sed 's/^/    /' 1>&2; }

function checkout {
  if [ -d "$DIR/repositories/ee" ] ; then
    cd "$DIR/repositories/ee"
    git reset --hard
    git pull origin master
  else
    git clone --single-branch "https://gitlab.com/gitlab-org/gitlab.git" "$DIR/repositories/ee"
    cd "$DIR/repositories/ee"
  fi
}

function set_vars {
  cd "$DIR/repositories/ee"
  HASH=$(git rev-parse HEAD)
  TMP_DIR="$DIR/tmp/$HASH"
  TIME=$(git show -s --format=%ct)
  RESULT_DIR="$DIR/results/ee/$TIME"
  cd "$DIR"
  rm -rf "$TMP_DIR"
  mkdir -p "$TMP_DIR"
  mkdir -p "$RESULT_DIR"
}

function install {
  cd "$DIR/repositories/ee"
  yarn install --pure-lockfile
  cd "$DIR"
}

function eslint {
  cd "$DIR/repositories/ee"
  node_modules/.bin/eslint . --ext .js,.vue -f "$DIR/slim-json-formatter.js" \
    --no-inline-config --report-unused-disable-directives \
    > "$TMP_DIR/eslint.json" || echo ""
  cd "$DIR"
}

function analysis_eslint {
  local pretty
  local total
  local top20
  local errors
  local warnings
  pretty=0

  log "Parsing eslint results"

  total='
    sort_by(.errorCount, .warningCount, .filePath) | reverse |
      .[] | [.errorCount, .warningCount, .filePath ] | map(tostring) |
      join("\t")
  '

  jq -r "$total" "$TMP_DIR/eslint.json" | sed "s#/.*repositories/ee/##g" > "$RESULT_DIR/eslint-files.txt"

  log "Aggregating eslint results"

  # shellcheck disable=SC2016
  top20='
    [.[].messages[].ruleId] |
      reduce .[] as $i ({}; setpath([$i]; getpath([$i]) + 1) ) |
      to_entries |
      sort_by(.value) | reverse | .[0:20] |
      from_entries
  '

  top20=$(jq "$top20" "$TMP_DIR/eslint.json")

  total=$(wc -l < "$RESULT_DIR/eslint-files.txt")
  pretty=$(awk '/^0\t0/ { count++ } END { print count }' "$RESULT_DIR/eslint-files.txt")
  errors=$(awk '{ sum += $1 } END { print sum + 0 }' "$RESULT_DIR/eslint-files.txt")
  warnings=$(awk '{ sum += $2 } END { print sum + 0 }' "$RESULT_DIR/eslint-files.txt")

  log "Eslint summary"

  echo '{"version": "ee"}' \
    | jq --arg total "$total" '.total = $total' \
    | jq --arg pretty "$pretty" '.pretty = $pretty' \
    | jq --arg errors "$errors" '.errors = $errors' \
    | jq --arg warnings "$warnings" '.warnings = $warnings' \
    | jq --arg 'hash' "$HASH" '.commit = $hash' \
    | jq --arg 'time' "$TIME" '.time = $time' \
    | jq --argjson 'top20' "$top20" '.top20 = $top20' \
    | tee "$RESULT_DIR/eslint-metadata.json"

  log "Downloading history from gitlab CI"

  download --output "$TMP_DIR/eslint-history.txt" \
    "https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/ee/eslint-history.txt"

  if ! grep -q "$HASH" "$TMP_DIR/eslint-history.txt" ; then
    printf "%s\\t%s\\t%s\\t%s\\t%s\\t%s\\n" "$CI_BUILD_DATE" "$total" "$pretty" "$errors" "$warnings" "$HASH" >> "$TMP_DIR/eslint-history.txt"
  fi

  sort -n -k1 "$TMP_DIR/eslint-history.txt" > "$RESULT_DIR/eslint-history.txt"

  tail -n 10 "$RESULT_DIR/eslint-history.txt"

}

function analysis_icons() {
  local tmpfile
  tmpfile="$TMP_DIR/icons.txt"
  tmpresult="$TMP_DIR/icons.json"
  cd "$DIR/repositories/ee"

  log "Find icon patterns"

  grep -E -r -n "icon[( ]([\"']).+?\1" {,ee/}app | tee "$tmpfile"
  grep -E -r -n "fa-\w+" {,ee/}app | tee -a "$tmpfile"

  log "Post-process icon patterns"

  node "$DIR/icons-postprocess.js" "$HASH" "$tmpfile" "$tmpresult"

  RESULT_DIR="$DIR/static/icons"
  rm -rf "$RESULT_DIR"
  mkdir -p "$RESULT_DIR"
  mv "$tmpresult" "$RESULT_DIR"

  log "Finished icon patterns"

  cd "$DIR"
}

function run_pipeline {
  log "Checking out gitlab-ee"
  checkout 2>&1 | indent
  set_vars
  log "Analysis of icons"
  analysis_icons 2>&1 | indent
  log "Installing dependencies"
  install 2>&1 | indent
  log "Running eslint"
  eslint 2>&1 | indent
  log "Formatting eslint results properly"
  analysis_eslint 2>&1 | indent
  log "Copying results to public dir"
  mkdir -p "static/ee"
  cp -r "$RESULT_DIR/." "static/ee"
  log "Finished running analysis on gitlab-ee"
}

function compile_static {
  cd "$DIR"
  log "Copying static page and zipping"
  yarn run generate 2>&1 | indent
  find dist -type f -print0 | xargs -0 gzip -f -k
  log "Finished build"
  tree dist/ | indent
}

log "Check if everything is alright"
yarn install 2>&1 | indent
yarn run lint 2>&1 | indent

HASH=
TMP_DIR=
RESULT_DIR=
TIME=

if [ "${CI:=false}" == "true" ]; then
  log "Overwriting build date"
  echo "{}" \
    | jq --argjson buildDate "$CI_BUILD_DATE" '.buildDate = $buildDate' \
    | jq --arg repoUrl "$CI_PROJECT_URL" '.repoUrl = $repoUrl' \
    > assets/data.json
else
  log "Not in CI environment, not overwriting build date"
fi

run_pipeline

compile_static

