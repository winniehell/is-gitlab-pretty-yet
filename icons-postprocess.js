#!/usr/bin/env node

const fs = require('fs');

function ignoredFinding({ path, match }) {
  // Ignore locales
  if (/locale\/\w\w(_\w\w)?/.test(path)) {
    console.warn(`Ignored locale path: ${path}`);
    return true;
  }

  // Ignore custom fontawesome css
  if (path.endsWith('/fontawesome_custom.scss')) {
    console.warn(`Ignore fontawesome custom css: ${path}`);
    return true;
  }

  // Ignore changelog and docs
  if (path.startsWith('CHANGELOG.md') || path.startsWith('doc/')) {
    console.warn(`Ignored documentation and changelog: ${path}`);
    return true;
  }

  // Ignore specs:
  if (path.startsWith('spec/') || path.startsWith('ee/spec')) {
    console.warn(`Ignored spec folders: ${path}`);
    return true;
  }

  // Ignore sprite icons
  if (
    (path.endsWith('.haml') || path.endsWith('.rb')) &&
    (match.includes('sprite_icon') ||
      match.includes('custom_icon') ||
      match.includes('emoji_icon') ||
      match.includes('statistic_icon') ||
      match.includes('tree_icon') ||
      match.includes('external_snippet_icon'))
  ) {
    console.warn(`Ignored sprite icon: ${path}`);
    return true;
  }

  // Ignore 2fa false positives
  if (match.includes('-2fa-')) {
    console.warn(`False positive (2FA): ${path}`);
    return true;
  }

  return false;
}

function canonicalIcons(arr) {
  const res = new Set(arr.filter(x => Boolean(x) && x.startsWith('fa-')));

  const flags = [];

  if (res.has('fa-spin') && res.has('fa-spinner')) {
    let spinner = 'fa-spinner fa-spin';
    res.delete('fa-spin');
    res.delete('fa-spinner');
    res.add(spinner);
  }

  if (res.has('fa-spin') && res.has('fa-refresh')) {
    let spinner = 'fa-refresh fa-spin';
    res.delete('fa-spin');
    res.delete('fa-refresh');
    res.add(spinner);
  }

  ['2x', 'fw', 'lg', 'spin', 'inverse'].forEach(flag => {
    if (res.has(`fa-${flag}`)) {
      res.delete(`fa-${flag}`);
      flags.push(`fa-${flag}`);
    }
  });

  return {
    icons: Array.from(res).sort(),
    flags,
  };
}

const CLASS_REGEX = /class=(["'])(.*?fa-[\w-]+.*?)\1/;
const ICON_REGEX = /icon[ (](["'])([\w- ]+?)\1/;
const SCSS_REGEX = /\.(fa-[\w-]+)(;|,| {)/;
const CLASS_MATCH = /fa-[\w-]+/gi;

function cleanFinding({ match, path }) {
  let cleanMatch = false,
    type = 'UNKNOWN';

  if (path.endsWith('.vue') && CLASS_REGEX.test(match)) {
    return {
      cleanMatch: canonicalIcons(match.match(CLASS_REGEX)[2].split(' ')),
      type: 'VUE_PLAIN_CLASS',
    };
  }

  if (path.endsWith('.js') && CLASS_REGEX.test(match)) {
    return {
      cleanMatch: canonicalIcons(match.match(CLASS_REGEX)[2].split(' ')),
      type: 'JS_PLAIN_CLASS',
    };
  }

  if (
    (path.endsWith('.rb') || path.endsWith('.haml')) &&
    ICON_REGEX.test(match)
  ) {
    return {
      cleanMatch: canonicalIcons(
        match
          .match(ICON_REGEX)[2]
          .split(' ')
          .map(x => 'fa-' + x)
      ),
      type: 'ICON_HELPER',
    };
  }

  if (path.endsWith('.haml') && match.includes('.fa-')) {
    return {
      cleanMatch: canonicalIcons(match.replace(/{.+$/, '').split('.')),
      type: 'HAML_PLAIN_CLASS',
    };
  }

  if (path.endsWith('.scss') && SCSS_REGEX.test(match)) {
    return {
      cleanMatch: canonicalIcons([match.match(SCSS_REGEX)[1]]),
      type: 'SCSS',
    };
  }

  if (
    (path.endsWith('.js') ||
      path.endsWith('.vue') ||
      path.endsWith('.js.haml')) &&
    CLASS_MATCH.test(match)
  ) {
    return {
      cleanMatch: canonicalIcons(match.match(CLASS_MATCH)),
      type: 'JS_OTHER',
    };
  }

  return { cleanMatch, type };
}

const processDocument = document => {
  const chunks = document.split(':');
  if (chunks.length >= 3) {
    const [path, line, ...rest] = chunks;

    const finding = {
      path,
      line: parseInt(line, 10),
      match: rest.join(':'),
    };

    if (ignoredFinding(finding)) {
      return [];
    }
    return [{ ...finding, ...cleanFinding(finding) }];
  }
  return [];
};

function main() {
  const hash = process.argv[2] || 'master';
  const inputFile = process.argv[3];
  const outputFile = process.argv[4];

  if (!inputFile) {
    throw new Error('Please define an input file');
  }

  if (!outputFile) {
    throw new Error('Please define an output file');
  }

  const input = fs.readFileSync(inputFile, 'utf8');

  const result = {
    hash,
    findings: input
      .split('\n')
      .flatMap(processDocument)
      .sort((a, b) => (a.path === b.path ? 0 : a.path < b.path ? -1 : 1)),
  };

  fs.writeFileSync(outputFile, JSON.stringify(result));
}

try {
  main();
  process.exit(0);
} catch (e) {
  console.error(e.message);
  process.exit(1);
}
